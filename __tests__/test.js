/* global test, describe, expect, beforeEach, beforeAll, afterAll, afterEach */
const TranslatedApp = require('../src/TranslatedApp')

let translatedApp
beforeAll(done => {
    translatedApp = new TranslatedApp()
    translatedApp.createMySQLConnection(err => {
        if (err) throw err
        done()
    })
})

beforeEach(done => {
    const connection = translatedApp._connection
    const projectTable =
        'CREATE TABLE IF NOT EXISTS projects (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255))'
    const jobTable =
        "CREATE TABLE IF NOT EXISTS jobs (id INT AUTO_INCREMENT PRIMARY KEY, creationDate DATE, price DECIMAL(10,2), status ENUM('in preparation', 'in progress', 'delivered', 'cancelled') NOT NULL)"
    const jobProjectTable =
        'CREATE TABLE IF NOT EXISTS projects_jobs (`id_project` INT NOT NULL,`id_job` INT NOT NULL, PRIMARY KEY(`id_project`, `id_job`))'
    connection.query(projectTable, err => {
        if (err) throw err
        connection.query(jobTable, err => {
            if (err) throw err
            connection.query(jobProjectTable, err => {
                if (err) throw err
                done()
            })
        })
    })
})

afterEach(done => {
    const connection = translatedApp._connection
    const dropProjects = 'DROP TABLE projects'
    const dropJobs = 'DROP TABLE jobs'
    const dropProjectsJobs = 'DROP TABLE projects_jobs'
    connection.query(dropProjects, err => {
        if (err) throw err
        connection.query(dropJobs, err => {
            if (err) throw err
            connection.query(dropProjectsJobs, err => {
                if (err) throw err
                done()
            })
        })
    })
})

describe('Test', () => {
    test('createJob#1 with a correct payload should work', done => {
        const job = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'in progress',
        }
        translatedApp.createJob(job, err => {
            expect(err).toBeUndefined()
            done()
        })
    })

    test('createJob#2 with an incorrect payload should not work', done => {
        const jobData = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'unknown',
        }
        translatedApp.createJob(jobData, err => {
            expect(err).toBeDefined()
            done()
        })
    })

    test('createProject#1', done => {
        const proejctData = {
            title: 'project-title',
            jobs: [1, 2, 3],
        }
        translatedApp.createProject(proejctData, err => {
            expect(err).toBeNull()
            done()
        })
    })

    test('getJobs#1 filtered by status', done => {
        const job1 = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'in progress',
        }
        const job2 = {
            creationDate: '2020-06-06',
            price: 30.3,
            status: 'in preparation',
        }
        const job3 = {
            creationDate: '2020-07-07',
            price: 40.4,
            status: 'in progress',
        }
        translatedApp.createJob(job1, err => {
            expect(err).toBeUndefined()
            translatedApp.createJob(job2, err => {
                expect(err).toBeUndefined()
                translatedApp.createJob(job3, err => {
                    expect(err).toBeUndefined()
                    translatedApp.getJobs(
                        { statusFilter: 'in progress', dateSort: 'NONE', priceSort: 'NONE' },
                        (err, res) => {
                            expect(err).toBeNull()
                            expect(res.length).toBe(2)
                            done()
                        }
                    )
                })
            })
        })
    })

    test('getJobs#2 ordered by descending date only', done => {
        const job1 = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'in progress',
        }
        const job2 = {
            creationDate: '2020-06-06',
            price: 30.3,
            status: 'in preparation',
        }
        const job3 = {
            creationDate: '2020-07-07',
            price: 40.4,
            status: 'in progress',
        }
        translatedApp.createJob(job1, err => {
            expect(err).toBeUndefined()
            translatedApp.createJob(job2, err => {
                expect(err).toBeUndefined()
                translatedApp.createJob(job3, err => {
                    expect(err).toBeUndefined()
                    translatedApp.getJobs(
                        { statusFilter: 'NONE', dateSort: 'DESC', priceSort: 'NONE' },
                        (err, res) => {
                            expect(err).toBeNull()
                            expect(res.length).toBe(3)
                            // TODO ho problemi nel parsare correttamente le date, come workaround uso il price
                            expect(res[0].price).toBe(40.4)
                            expect(res[1].price).toBe(30.3)
                            expect(res[2].price).toBe(20.2)
                            done()
                        }
                    )
                })
            })
        })
    })

    test('getJobs#3 ordered by ascending price only', done => {
        const job1 = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'in progress',
        }
        const job2 = {
            creationDate: '2020-06-06',
            price: 30.3,
            status: 'in preparation',
        }
        const job3 = {
            creationDate: '2020-07-07',
            price: 40.4,
            status: 'in progress',
        }
        translatedApp.createJob(job1, err => {
            expect(err).toBeUndefined()
            translatedApp.createJob(job2, err => {
                expect(err).toBeUndefined()
                translatedApp.createJob(job3, err => {
                    expect(err).toBeUndefined()
                    translatedApp.getJobs(
                        { statusFilter: 'NONE', dateSort: 'NONE', priceSort: 'ASC' },
                        (err, res) => {
                            expect(err).toBeNull()
                            expect(res.length).toBe(3)
                            expect(res[0].price).toBe(20.2)
                            expect(res[1].price).toBe(30.3)
                            expect(res[2].price).toBe(40.4)
                            done()
                        }
                    )
                })
            })
        })
    })

    test('getJobs#3 sorting and ordering all togheter', done => {
        const job1 = {
            creationDate: '2020-05-05',
            price: 20.2,
            status: 'in progress',
        }
        const job2 = {
            creationDate: '2020-06-06',
            price: 30.3,
            status: 'in preparation',
        }
        const job3 = {
            creationDate: '2020-07-07',
            price: 40.4,
            status: 'in progress',
        }
        translatedApp.createJob(job1, err => {
            expect(err).toBeUndefined()
            translatedApp.createJob(job2, err => {
                expect(err).toBeUndefined()
                translatedApp.createJob(job3, err => {
                    expect(err).toBeUndefined()
                    translatedApp.getJobs(
                        { statusFilter: 'in progress', dateSort: 'DESC', priceSort: 'ASC' },
                        (err, res) => {
                            expect(err).toBeNull()
                            expect(res.length).toBe(2)
                            expect(res[0].price).toBe(40.4)
                            expect(res[1].price).toBe(20.2)
                            done()
                        }
                    )
                })
            })
        })
    })
})
