# NOTA

Ciao Damiano, 
ho impiegato molto tempo a realizzare il progetto che ti sto per linkare, onestamente pensavo fosse un esercizio diverso, la difficoltà maggiore che ho incontrato è che l'esercizio è molto incentrato su una corretta implementazione di un database MySQL, cosa che personalmente non ho mai fatto, mi aspettavo qualcosa di più generico (ho scritto molti moduli che utilizzano MySQL ma sempre nel ruolo di consumer).

Ho speso quindi diverso tempo cercando di progettare il db in modo corretto ma alla fine ho desistito perchè di certo non si può improvvisare una corretta implementazione di un db relazionale.


Fatta questa pessima premessa ti illustro ciò che ho fatto:

Il modulo presuppone l'esistenza di un database così strutturato:

Tabella projects con id, title
Tabella jobs con id, creationDate, price, status
Tabella projects_jobs con id_project, id_job

Il modulo può essere usato sia come libreria node che come server express


# API
La API della libreria node è

## createJob(jobData, callback)

Crea un job nella tabella jobs
jobData è un oggetto con i campi

creationDate (data)
price (decimale)
status (enum: 'in preparation', in progress', 'delivered', 'cancelled')

## createProject(projectData, callback)

Crea un project nella tabella project e aggiorna la tabella projects_jobs con le coppie opportune
projectData è un oggetto con i campi
title (stringa)
jobs (array di id di jobs, la cui referenzialità NON è stata implementata)

## getJobs(query, callback)

Ritorna un array di jobs
query è un oggetto che richiede sempre la presenza di questi campi
statusFilter (valori ammessi 'in preparation', in progress', 'delivered', 'cancelled', 'NONE')
dateSort (valori ammessi 'ASC', 'DESC', 'NONE')
priceSort (valori ammessi 'ASC', 'DESC', 'NONE')

questo metodo funziona sia da filtro che da ordinamento

## updateJob(jobId, newStatus, callback)
Per mancanza di tempo non è stato implementato



# ROUTING

Le route del server express (in ascolto su porta 8008) sono

## create-job
E' una POST, il body deve rispettare la API sopra citata

## create-project
E' una POST, il body deve rispettare la API sopra citata

## get-jobs
E' una GET, i query parameters devono rispettare la API sopra citata


Sempre per mancanza di tempo non ho terminato l'implementazione del sistema dockerizzato che puoi vedere abbozzata nel Dockerfile e nel docker-compose.yml


# TEST
Cosa si può fare con lo stato attuale delle cose:

## TEST AUTOMATICI
Ho creato una suite di test con jest (usa un container con mysql)

Per farlo bisogna:
avere un docker daemon attivo
scaricare il repo
installare i moduli con **npm install**
lanciare il container con **npm run-script mysql:start** e attendere che il server sia up
lanciare i test con **npm test**

i test creano le tabelle opportune


## TEST DELLE ROUTE
Sempre con il container di mysql in attività lanciare **npm start-local**, verranno create le tabelle opportune e un server express sarà in ascolto sulla porta 8008
Con un qualunque sistema è possibile chiamare le routes descritte sopra

Un saluto!
Luigi
