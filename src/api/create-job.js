const createJob = (connection, { creationDate, price, status }, done) => {
    if (!connection) return done(new Error('missing connection'))
    if (!creationDate) return done(new Error('missing creationDate'))
    if (!price) return done(new Error('missing price'))
    if (!status) return done(new Error('missing status'))

    const sql = `INSERT INTO jobs(creationDate, price, status) VALUES(?, ?, ?)`

    connection.query(sql, [creationDate, price, status], err => {
        if (err) return done(err)
        done()
    })
}

module.exports = createJob
