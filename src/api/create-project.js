const _createProjectJobArray = (projectId, jobs) => {
    let res = []
    jobs.forEach(job => {
        res.push([projectId, job])
    })
    return res
}

const createProject = (connection, { title, jobs }, done) => {
    if (!connection) return done(new Error('missing connection'))
    if (!title) return done(new Error('missing title'))
    if (!jobs) return done(new Error('missing jobs'))

    if (!Array.isArray(jobs)) return done(new Error('jobs is not an array'))
    if (!jobs.length) return done(new Error('jobs is empty'))

    // TODO
    // Non c'e' controllo referenziale

    // Crea il project in projects
    // Crea le relazioni project <--> job nella tabella projects_jobs
    const sql1 = `INSERT INTO projects(title) VALUES(?)`
    const sql2 = `INSERT INTO projects_jobs(id_project, id_job) VALUES ?`

    connection.query(sql1, [title], (err, results) => {
        if (err) return done(err)

        const projectId = results.insertId
        const projectArrayTuples = _createProjectJobArray(projectId, jobs)

        connection.query(sql2, [projectArrayTuples], done)
    })
}

module.exports = createProject
