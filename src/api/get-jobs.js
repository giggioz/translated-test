const getJobs = (connection, { statusFilter, dateSort, priceSort }, done) => {
    if (!connection) return done(new Error('missing connection'))
    if (!statusFilter) return done(new Error('missing statusFilter'))
    if (!dateSort) return done(new Error('missing dateSort'))
    if (!priceSort) return done(new Error('missing priceSort'))

    let sql = `SELECT * FROM jobs `

    if (statusFilter !== 'NONE') sql += `WHERE status = '${statusFilter}' `

    if (dateSort !== 'NONE' && priceSort !== 'NONE')
        sql += `ORDER BY creationDate ${dateSort}, price ${priceSort}`

    if (dateSort !== 'NONE' && priceSort === 'NONE') sql += `ORDER BY creationDate ${dateSort}`

    if (dateSort === 'NONE' && priceSort !== 'NONE') sql += `ORDER BY price ${priceSort}`

    connection.query(sql, (err, res) => {
        if (err) return done(err)
        done(null, res)
    })
}

module.exports = getJobs
