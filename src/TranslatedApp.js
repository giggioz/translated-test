const pckg = require('../package.json')

const createMySQLConnection = require('./create-mysql-connection')
const createServerExpress = require('./express/create-server-express')

const api = require('./api')

class TranslatedApp {
    version() {
        return pckg.version
    }

    createMySQLConnection(done) {
        createMySQLConnection((err, connection) => {
            if (err) return done(err)
            this._connection = connection
            done()
        })
    }

    createServerExpress(done) {
        createServerExpress(this._connection, done)
    }

    createJob(jobData, done) {
        api.createJob(this._connection, jobData, done)
    }

    createProject(projectData, done) {
        api.createProject(this._connection, projectData, done)
    }

    getJobs(options, done) {
        api.getJobs(this._connection, options, done)
    }
}
module.exports = TranslatedApp
