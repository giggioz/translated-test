const express = require('express')
const bodyParser = require('body-parser')

const routes = require('./routes')

const createServerExpress = (connection, done) => {
    const app = express()
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))

    const port = process.env.PORT || 8008

    app.get('/test', (req, res) => {
        res.status(200).send({ success: true })
    })

    app.post('/create-job', (req, res) => {
        routes.createJob(connection, req, res)
    })

    app.post('/create-project', (req, res) => {
        routes.createProject(connection, req, res)
    })

    app.get('/get-jobs', (req, res) => {
        routes.getJobs(connection, req, res)
    })

    app.listen(port, err => {
        if (err) return done(err)
        console.log('Translated App Server Listening On Port ' + port)
        done()
    })
}

module.exports = createServerExpress
