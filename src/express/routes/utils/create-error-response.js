const createErrorResponse = err => ({ success: false, err })

module.exports = createErrorResponse
