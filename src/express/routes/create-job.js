const createErrorResponse = require('./utils/create-error-response')
const api = require('../../api')

const createJob = (connection, req, res) => {
    api.createJob(connection, req.body, err => {
        if (err) return res.status(400).send(createErrorResponse(err.message))
        return res.status(200).send({ success: true })
    })
}

module.exports = createJob
