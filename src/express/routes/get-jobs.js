const createErrorResponse = require('./utils/create-error-response')
const api = require('../../api')

const getJobs = (connection, req, res) => {
    api.getJobs(connection, req.query, (err, jobs) => {
        if (err) return res.status(400).send(createErrorResponse(err.message))
        return res.status(200).send({ success: true, data: jobs })
    })
}

module.exports = getJobs
