const mysql = require('mysql')

const connectionOptions = {
    host: process.env.MYSQL_HOST || '127.0.0.1',
    port: process.env.MYSQL_PORT || '3306',
    user: process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_PASS || 'xxxxxx',
    database: process.env.MYSQL_DB || 'translated_db',
}

const createMySQLConnection = done => {
    const connection = mysql.createConnection(connectionOptions)

    connection.connect(err => {
        if (err) return done(err)
        return done(null, connection)
    })
}

module.exports = createMySQLConnection
