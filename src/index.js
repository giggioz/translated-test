const TranslatedApp = require('./TranslatedApp')

let translatedApp = new TranslatedApp()

translatedApp.createMySQLConnection(err => {
    if (err) throw err
    translatedApp.createServerExpress(err => {
        if (err) throw err
    })
})
