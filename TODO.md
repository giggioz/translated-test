#TODO
- migliorare la leggibilità con async/await
- implementare i metodi mancanti
- terminare la configurazione per Docker

Alternativa a Docker Compose
- Creare un unico container con ubuntu e installare come servizi mysql e l'app node

[...header...]
FROM stackbrew/ubuntu:12.04 #or use ubuntu-upstart:12.04
MAINTAINER BPetkov  

RUN apt-get update -qq 

# Mysql
RUN apt-get install -y mysql-server-5.5  
ADD my.cnf /etc/mysql/conf.d/my.cnf 
RUN chmod 664 /etc/mysql/conf.d/my.cnf 
ADD run /usr/local/bin/run 
RUN chmod +x /usr/local/bin/run  

# Other stuff
RUN apt-get -y install nodejs
[...]
VOLUME ["/var/lib/mysql"] 
EXPOSE 3306 
EXPOSE .......
CMD ["/sbin/init"]

