const TranslatedApp = require('../src/TranslatedApp')

let translatedApp = new TranslatedApp()

translatedApp.createMySQLConnection(err => {
    if (err) throw err

    const connection = translatedApp._connection
    const projectTable =
        'CREATE TABLE IF NOT EXISTS projects (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255))'
    const jobTable =
        "CREATE TABLE IF NOT EXISTS jobs (id INT AUTO_INCREMENT PRIMARY KEY, creationDate DATE, price DECIMAL(10,2), status ENUM('in preparation', 'in progress', 'delivered', 'cancelled') NOT NULL)"
    const jobProjectTable =
        'CREATE TABLE IF NOT EXISTS projects_jobs (`id_project` INT NOT NULL,`id_job` INT NOT NULL, PRIMARY KEY(`id_project`, `id_job`))'
    connection.query(projectTable, err => {
        if (err) throw err
        connection.query(jobTable, err => {
            if (err) throw err
            connection.query(jobProjectTable, err => {
                if (err) throw err
                translatedApp.createServerExpress(err => {
                    if (err) throw err
                })
            })
        })
    })
})
